\documentclass[]{article}
\usepackage{hyperref}

\usepackage{float}
\usepackage{sidecap}
\usepackage{graphicx}
\graphicspath{ {./images/} }

\usepackage{amssymb}
\usepackage{amsmath}

%opening
\title{Spatial Transformer Networks}
\author{Samuel Fabo}
\date{\today}

\begin{document}

\maketitle

\section{Prerequisites}
\begin{enumerate}
	
	\item Intro to Fully Connected Networks (FCN) \href{https://towardsdatascience.com/under-the-hood-of-neural-networks-part-1-fully-connected-5223b7f78528}{TowardsDataScience.com}
	
	\item Intro to Convolutional Neural Networks (CNN) \cite{726791}, if you missed some of our talks, see \href{https://www.youtube.com/watch?v=FmpDIaiMIeA}{YouTube}
		
	\item \emph{Information about invariance and equivariance in CNN: \href{https://www.quora.com/What-is-the-difference-between-equivariance-and-invariance-in-Convolution-neural-networks}{Quora}}
	
	\item A simple explanation of the basic transformations \href{http://graphics.cs.cmu.edu/courses/15-463/2006_fall/www/Lectures/warping.pdf}{CMG course}.
	\begin{itemize}
		\item If you want to feel nostalgy, see \href{https://courses.fit.cvut.cz/BI-LIN/media/buildbot/lin-text.pdf}{5.5 \& 5.7}
	\end{itemize}
	
\end{enumerate}


\section{Overview}
CNNs are not invariant to rotation, scale or more general affine transformations. Spatial Transformer (ST) Network is a differentiable module that can be inserted anywhere in CNN architecture to increase its geometric invariance. 

It effectively gives the network the ability to spatially transform feature maps (FM) or input images at no extra data or supervision cost. For example, it can crop a region of interest, scale and correct the orientation of given FM or image. Regression can be used here, thanks to differentiability, so we can call this module learnable.

\begin{SCfigure}
	\label{fig:intu}
	\centering
	\caption{Distorted MNIST example from the original paper. From left to right: Original image, target object detected by ST Network, transformed image, classification prediction produced by subsequent FCN.}
	\includegraphics[width=0.5\textwidth]{intuition}
\end{SCfigure}

\clearpage

\section{Spatial Transformers}
ST boils down into three main components, shown here:

\begin{figure}[h]
	\label{fig:arch}
	\caption{The architecture of a spatial transformer module}
	\includegraphics[width=0.9\textwidth]{stn}
	\centering
\end{figure}


\subsection{Localisation network}\label{sub:localnet}
Localisation network takes the input FM: $U \in \mathbb{R}^{H \times W \times C}$ with height $H$ width $W$ and $C$ channels. Then it outputs parameters $\theta$ of transformation $T_\theta$.
 
This network can be a regular CNN or FCN which should regress the transformation parameters $\theta$. These are never learned explicitly from the dataset. Instead, the network learns parameters $\theta$ that enhance the global accuracy.

\subsection{Parameterised Sampling Grid (Grid Generator)}
This takes the transformation parameters $\theta$ and generates a grid $G = \{G_i\}$ of coordinates (pixels) $G_i = (x_i^t, y_i^t)$ forming output FM  $V \in \mathbb{R}^{H' \times W' \times C}$, where $H'$ and $W'$ are the height and width of the grid, and $C$ is the number of channels, which is the same in the input and output.
Now assume $T_\theta$ as 2D affine transformation $A_\theta$. In this case, the pointwise transformation is
\begin{equation} \label{eq:1}
\begin{pmatrix}
x_i^s \\ y_i^s
\end{pmatrix}
 = T_\theta (G_i) = A_\theta \begin{pmatrix}
x_i^t \\ y_i^t \\ 1
\end{pmatrix}
= \begin{bmatrix}
\theta_{11} & \theta_{12} & \theta_{12} \\
\theta_{21} & \theta_{22} & \theta_{23}
\end{bmatrix}
\begin{pmatrix}
x_i^t \\ y_i^t \\ 1
\end{pmatrix}
\end{equation}
where $(x_i^s, y_i^s)$ are the source coordinates in the input FM.
The source/target transformation and sampling are equivalent to the standard texture mapping and coordinates used in graphics.\\
Equation \ref{eq:1} allows cropping, translation, rotation, scale, and skew to be applied to the input feature map and requires only 6 parameters to be produced by the localisation network. This equation also allows cropping, thanks to linear algebra magic. Comparison between identity and transformation in fig. \ref{fig:3}

\begin{figure}[h]
	\label{fig:3}
	\caption{The effect of \ref{eq:1} on the grid compared to the identity transformation}
	\includegraphics[width=10cm]{transformation}
	\centering
\end{figure}

$T_\theta$ can also be more general. The crucial property of the transformation is differentiability with respect to the parameters. Differentiability of the model allows the gradients to be backpropagated within the network, which means, it can find optimal parameters.

Structured and differentiable transformations (a superset of all fundamental transformations you can think of): $T_\theta = M_\theta B$, where $B$ is a target grid, and $M_\theta$ is a matrix parametrised by $\theta$. In this case, it is possible to learn $\theta$, but also $B$.

\subsection{Differentiable Image Sampling (Sampler)}

Here comes some numerical analysis magic soup.

The sampler uses the parameters $\theta$ of the transformation $T_\theta$ and applies them (this means $T_\theta (G)$) to the input FM. For each $(x_i^s, y_i^s)$ coordinate in $T_\theta (G)$ a sampling kernel is applied, to get the target coordinates $(x_i^t, y_i^t)$.

\begin{equation} \label{eq:2}
\begin{split}
V_i^c = \sum_{n}^{H} \sum_{m}^{W} U_{nm}^c k(x_i^s - m ; \phi_x) k(y_i^s - n ; \phi_y) \\
\forall i \in [1 \dots H'W'], \forall c \in [1 \dots C]
\end{split}
\end{equation}


In \ref{eq:2} $\phi_x$ and $\phi_y$ are the parameters of generic sampling kernel $k(\cdot ; \cdot)$ which defines the image interpolation (e.g., bilinear). \\ $U_{nm}^c$ is a value on input FM's coordinate on channel $c$, \\
$V_i^c$ is a value on output FM's coordinate on channel $c$.\\

Interpolation is required as we need to map the result of sampling (fractions of pixels) to pixel values (integers). This technique is beyond this course so that I will leave this for numerical analysts. What happens here is an approximation, how should $V$ space look like, and make it differentiable, so we can gradient descend and find an optimal solution - best transformation matrix $M_\theta$.

\clearpage

\section{Experiments}

Showing experiments comparing traditional CNNs with ST + CNN.
\begin{figure}[h]
	\label{fig:mnist}
	\caption{\textit{Left}: table with percentage errors for different models on different distorted MNIST datasets. \textbf{R}: rotation, \textbf{RTS}: rotation, translation, and scale, \textbf{P}: projective distortion, \textbf{E}: elastic distortion. \textbf{TPS}: thin plate spline. \textit{Right}: (a) some of the input images where simple CNN fails, but ST-CNN does well, (b) visualising $T_\theta (G)$, (c) output of the ST.}
	\includegraphics[width=1\textwidth]{mnist}
	\centering
\end{figure}

\begin{figure}[h]
	\label{fig:house}
	\caption{\textit{Left}: sequence error on Street View House Numbers dataset, multi-digit recognition on $64 \times 64px$ and inflated 128px with more background. Tried on Maxout CNN \cite{DBLP:journals/corr/GoodfellowBIAS13}, DRAM \cite{DBLP:journals/corr/BaMK14} \textit{Right}: (a) The schematic of the ST-CNN Multi-model. (b) The result of multiplying out the affine transformations predicted by the four spatial transformers in ST-CNN Multi visualised on the input image}
	\includegraphics[width=1\textwidth]{house}
	\centering
\end{figure}


\section{Conclusion}
\begin{itemize}
	\item We can see ST networks as an alternative to image augmentation, which is a default way to achieve spatial invariance for a CNN.
	
	\item The paper states that the ST network module can be inserted anywhere and as many times as needed in the resulting ST-CNN chain.
	
	\item While CNNs provide an incredibly strong baseline, we see gains in accuracy using spatial transformers across multiple tasks, resulting in better performance.
\end{itemize}

\medskip

\bibliographystyle{unsrt}
\bibliography{/Users/samuelfabo/Documents/LITERATURE-BibTeX/library.bib}

\end{document}
